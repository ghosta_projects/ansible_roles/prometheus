Prometheus
=========

This role will install prometheus on your server.

platforms
------------

Ubuntu:     
  versions:     
    - focal amd64 (20.04)     
    - jammy amd64 (22.04)
